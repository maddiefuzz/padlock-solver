package com.cleverthis.interview;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for DumbBruteSolver class
 *
 * Tests that lexicographically ordered permutations are properly calculated, and that the solver works
 */
public class DumbBruteSolverTest extends SolutionTestBase {

    /**
     * Overridden method that tests in SolutionTestBase.class expect defined and calls.
     * Creates a DumbBruteSolver and calls the solve() method.
     * @param padlock The padlock to solve
     */
    @Override
    protected void solve(PadlockAdapter padlock) {
        new DumbBruteSolver().solve(padlock);
    }
    /**
     * Check whether a 7-button padlock can be solved
     */
    @Test
    protected void testSolver() {
        DumbBruteSolver dumbSolver = new DumbBruteSolver();
        PadlockJavaAdapter padlock = new PadlockJavaAdapter(7);

        dumbSolver.solve(padlock);

        assertTrue(padlock.isPasscodeCorrect());
    }

    /**
     * Check whether the next permutation is calculated correctly
     */
    @Test
    protected void testOnePermutation() {
        DumbBruteSolver dumbSolver = new DumbBruteSolver();

        Integer[] permutation = new Integer[] {1, 2, 3, 4};
        Integer[] correctPermutation = new Integer[] {1, 2, 4, 3};

        dumbSolver.calculateNextPermutation(permutation);

        assertArrayEquals(permutation, correctPermutation);
    }

    /**
     * Check whether two consecutive permutations are correct
     */
    @Test
    protected void testTwoPermutations() {
        DumbBruteSolver dumbSolver = new DumbBruteSolver();

        Integer[] permutation = new Integer[] {1, 2, 3, 4};
        Integer[] correctPermutation = new Integer[] {1, 3, 2, 4};

        for(int i = 0; i < 2; i++) {
            dumbSolver.calculateNextPermutation(permutation);
        }

        assertArrayEquals(permutation, correctPermutation);
    }

    /**
     * Check whether 23 consecutive permutations are correct
     */
    @Test
    protected void test23Permutations() {
        DumbBruteSolver dumbSolver = new DumbBruteSolver();

        Integer[] permutation = new Integer[] {1, 2, 3, 4};
        Integer[] correctPermutation = new Integer[] {4, 3, 2, 1};

        for(int i = 0; i < 23; i++) {
            dumbSolver.calculateNextPermutation(permutation);
        }

        assertArrayEquals(permutation, correctPermutation);
    }

    /**
     * Check whether the 24th permutation returns false, signifying we've exhausted all possible permutations
     * for a list of size 4
     */
    @Test
    protected void test24Permutations() {
        DumbBruteSolver dumbSolver = new DumbBruteSolver();

        Integer[] permutation = new Integer[] {1, 2, 3, 4};

        for(int i = 0; i < 23; i++) {
            assertTrue(dumbSolver.calculateNextPermutation(permutation));
        }

        assertFalse(dumbSolver.calculateNextPermutation(permutation));

    }
}
