package com.cleverthis.interview;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Tests for WriteAwareBruteSolver class
 *
 * These tests do not keep a long-lived reference to WriteAwareBruteSolver, instead opting
 * to reconstruct the permutation tree for every test. This is a trade-off between performance
 * and avoiding any inadvertent behavior changes from state persisting between tests.
 */
public class WriteAwareBruteSolverTest extends SolutionTestBase {

    /**
     * Overridden method that tests in SolutionTestBase.class expect defined and calls.
     * Creates a WriteAwareBruteSolver and calls the solve() method.
     * @param padlock The padlock to solve
     */
    @Override
    protected void solve(PadlockAdapter padlock) {
        new WriteAwareBruteSolver(padlock.getNumpadSize()).solve(padlock);
    }

    /**
     * Tests whether the solver can brute-force a 7-numpad padlock.
     */
    @Test
    protected void testSolver() {
        Integer numpadSize = 7;
        WriteAwareBruteSolver writeAwareSolver = new WriteAwareBruteSolver(numpadSize);
        PadlockJavaAdapter padlock = new PadlockJavaAdapter(numpadSize);

        writeAwareSolver.solve(padlock);

        assertTrue(padlock.isPasscodeCorrect());
    }

    /**
     * Tests whether a numpad-size of 4 is correctly expanded to 24 possible permutations in the underlying
     * tree
     */
    @Test
    protected void testTreeSize() {
        Integer numpadSize = 4;
        WriteAwareBruteSolver writeAwareSolver = new WriteAwareBruteSolver(numpadSize);
        PadlockJavaAdapter padlock = new PadlockJavaAdapter(4);

        writeAwareSolver.solve(padlock);

        assertEquals(writeAwareSolver.getTreeSize(), 24);
    }
}
