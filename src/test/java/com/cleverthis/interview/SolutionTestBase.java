package com.cleverthis.interview;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * This is a base class for verifying the correctness of the solution.
 */
public abstract class SolutionTestBase {
    
    /**
     * Implement your solution in this function.
     * */
    protected abstract void solve(PadlockAdapter padlock);

    protected void verify(int numpadSize) {
        PadlockAdapter padlock = new PadlockJavaAdapter(numpadSize);
        solve(padlock);
        assertTrue(padlock.isPasscodeCorrect());
    }

    /**
     * Tests padlocks with numpad sizes of 1 to 7. This test runs for every class that extends this SolutionTestBase
     * abstract class.
     */
    @Test
    void verify1to7() {
        for (int i = 1; i <= 7; i++) {
            verify(i);
        }
    }
}
