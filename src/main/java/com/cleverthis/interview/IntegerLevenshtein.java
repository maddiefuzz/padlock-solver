package com.cleverthis.interview;

import org.apache.commons.text.similarity.LevenshteinDistance;

/**
 * Holds a single possible padlock passcode permutation in an integer array. This class
 * overloads the toString() method and is comparable, using its string value to calculate the levenshtein distance and
 * use those distance values for sorting into a data structure.
 */
public class IntegerLevenshtein implements Comparable<IntegerLevenshtein> {
    private Integer[] integerData;
    private Integer size;

    /**
     * Creates a new IntegerLevenshtein
     * @param size The number of integers (keypad size) of the passcode
     */
    public IntegerLevenshtein(int size) {
        this.integerData = new Integer[size];
        this.size = size;
    }

    /**
     * Sets the integer data array
     * @param integerData The new integer data
     */
    public void setIntegerData(Integer[] integerData) {
        this.integerData = integerData.clone();
    }

    /**
     * Gets the integer data array
     * @return the integer data
     */
    public Integer[] getIntegerData() {
        return this.integerData;
    }

    /**
     * Casts each integer to a string and concatenates them together into a single string.
     * For example, an internal integer array of [1,2,3,4] will be returned as the string "1234".
     * @return A string representation of the integer array.
     */
    public String toString() {
        String temp = new String();
        for(int i = 0; i < size; i++) {
            temp = temp.concat(integerData[i].toString());
        }
        return temp;
    }

    /**
     * Overridden compareTo method that compares the calculated levenshtein distance between two IntegerLevenshtein objects
     * @param otherInteger the object to be compared.
     * @return The levenshtein distance between the two objects.
     */
    @Override
    public int compareTo(IntegerLevenshtein otherInteger) {
        LevenshteinDistance levenshtein = LevenshteinDistance.getDefaultInstance();
        return levenshtein.apply(this.toString(), otherInteger.toString());
    }
}
