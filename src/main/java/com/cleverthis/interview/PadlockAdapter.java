package com.cleverthis.interview;

/**
 * This defines the contract that padlocks must conform to.
 * Concrete implementations will be adapted to this interface contract through concrete adapter classes.
 */
public interface PadlockAdapter {

    /**
     * Get the size of the padlock's physical number pad
     *
     * @return A count of the physical buttons on the padlock
     */
    int getNumpadSize();

    /**
     *  Write key presses to the input buffer of the padlock
     * @param address The position / index of the button that is pressed. For example, address 0 is the first button pressed.
     * @param keyIndex The value of the button that is pressed. Cannot be greater than the numpad size, as the buttons increment
     *                 sequentially
     * @return The old value of keyIndex at the inputted address
     */
    Integer writeInputBuffer(int address, int keyIndex);

    /**
     * Check whether the inputted password is correct
     * @return True if password is correct, false otherwise
     */
    boolean isPasscodeCorrect();

    /**
     * Returns the write counter
     * @return The number of times a write operation has occurred
     */
    long getWriteCounter();

    /**
     * Returns the check counter
     * @return The number of times the password has been checked for correctness
     */
    long getCheckCounter();

    /**
     * Resets both the check and write counters
     */
    void resetCounter();
}
