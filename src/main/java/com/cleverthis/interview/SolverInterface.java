package com.cleverthis.interview;

/**
 * Defines the contract for padlock-cracking solver implementations
 */
public interface SolverInterface {

    /**
     * Solves the padlock passed in that conforms to the PadlockAdapter interface
     * @param padlockAdapter the padlock object to solve
     */
    void solve(PadlockAdapter padlockAdapter);
}
